#include <stdio.h>

int main(int argc, char **argv) {
    char name[20];

    printf("What is the name of the entity? ");
    fgets(name, 20, stdin);

    printf("You chose: %s", name);

    return 0;
}
